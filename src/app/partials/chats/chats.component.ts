import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss'],
})
export class ChatsComponent implements OnInit {

  @Input('item') k:number;

  constructor() { }

  ngOnInit() {}

}
