import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { StatusComponent } from '../partials/status/status.component';
import { ChatsComponent } from '../partials/chats/chats.component';
import { CallsComponent } from '../partials/calls/calls.component';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage,StatusComponent,ChatsComponent, CallsComponent]
})
export class TabsPageModule {}
