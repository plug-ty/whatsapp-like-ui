import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  @ViewChild('myslider',{read:false,static:true}) myslider: IonSlides;
  page:any = 0;
  searching:boolean = false;
  sliderConfig = {
     spaceBetween:0,
     centeredSlide:false,
     speed:300
  }

  constructor(private menuCtrl:MenuController, private router:Router) { }

  ngOnInit() {
    
  }


  toggleMenu(){
    this.menuCtrl.toggle();
  }

  openSearch(){
    this.searching = true;
  }

  closeSearch(){
    this.searching = false;
  }

  selectTab(page:number){
    this.myslider.slideTo(page);
  }

  async swipePages(){
    let index = this.myslider.getActiveIndex();
    index.then((i)=> this.page = i);
  }

  detailChat(){
    this.router.navigate(['single-chat']);
  }

}
